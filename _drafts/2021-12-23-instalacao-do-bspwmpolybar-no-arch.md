---
title: "Instalação do BSPWM / Polybar no Arch"
date: 2021-12-23 17:19:19
key: bspwm
image: '/assets/img/'
description:
tags: bspwm, arch, polybar
---

## Instalação do BSPWM e Polybar no Arch Linux

#### Instalação inicial

```
sudo pacman -Syy base-devel git vim bspwm sxhkd termite nitrogen picom lightdm lightdm-gtk-greeter firefox pcmanfm dmenu arandr archlinux-wallpaper --noconfirm --needed
```

#### Instalar o Paru

```
git clone https://aur.archlinux.org/paru.git
cd paru
makepkg -si
```

#### Instalação do polybar

```
paru -S polybar pacman-contrib ttf-awesome-fonts siji-git pulseaudio alsa-utils termite
```

#### Criação dos diretórios

```
mkdir -p ~/.config/{bspwm,sxhkd,polybar}
```

#### Copiando os arquivos

```
cp /usr/share/doc/bspwm/examples/bspwmrc ~/.config/bspwm/
cp /usr/share/doc/bspwm/examples/sxhkdrc ~/.config/sxhkd/
```

#### Editando os arquivos

Alterar no arquivo bspwmrc:

```
bspc config window_gap		5
```

e mude de Chromium para firefox.

Alterar no arquivo sxhkdrc:

```
termite

super + d (program launcher)
```

#### Habilitar o Lightdm

```
sudo systemctl enable lightdm
```

#### Reinicia a maquina

```
reboot
```

### Após o primeiro boot

#### Configurar a resolução de vídeo

```
xrandr -s 1360x768
```

#### Criando o arquivo de configuração do Polybar

```
vim ~/.config/polybar/launcher.sh
```

Adicionar as seguintes linhas ao arquivo:

```
#!/bin/bash

# Terminate already running bar instances
killall -q polybar
# If all your bars have ipc enabled, you can also use
# polybar-msg cmd quit

# Launch Polybar, using default config location ~/.config/polybar/config
polybar example 2>&1 | tee -a /tmp/polybar.log & disown

echo "Polybar launched..."
```

```
chmod +x ~/.config/polybar/launch.sh
```

edite o ~/.config/bspwm/bspwmrc e adicione a seguinte linha:

```
$HOME/.config/polybar/launch.sh
```

#### Config do Polybar

Copie o conteúdo do arquivo Config neste [link](https://gitlab.com/fnogueira/bspwm-configs/-/blob/master/polybar/config) e cole no arquivo abaixo:

```
~/.config/polybar/config
```

#### Configurando o monitor pelo arandr

Execute o arandr, clique em Outputs > Resolutions e escolha a resolução do seu monitor.

Em seguida, clique na opção de salvar e dentro da pasta .screenlayout, salve o arquivo como monitor.sh

#### Arquivo xprofile

Na raiz, crie o arquivo .xprofile:

```
vim ~/.xprofile
```

Adicione as seguintes linhas, e salve:

```
# Papel de parede
nitrogen --random --set-zoom-fill &

# Compositor
picom -f &

# Resolução da tela (Conteúdo do arquivo monitor.sh)
xrandr --output Virtual-1 --primary --mode 1360x768 --pos 0x0 --rotate normal
```

Reinicie a máquina!

#### Configurando o Papel de parede

Execute o nitrogen e clique em:

Preferences > Add e escolha a pasta:

usr/share/backgrounds/archlinux

Escolha o papel de parede e clique em Apply.

#### Conhecendo os atalhos

Navegue pelo arquivo ~/.config/sxhkd/sxhkdrc e conheça mais dos atalhos do sistema

Acrescente na opção Applications, os seguintes atalhos:

```
ctrl + alt + f
	pcmanfm

ctrl + alt + g
	gimp
```

Agora é só curtir! ;)
