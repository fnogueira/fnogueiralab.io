---
layout: article
comments: true
key: shellscript
title: "Dica rápida #1 - Shell script"
date: 2021-12-17 13:17:02
image: '/assets/img/'
description:
tags: shell
---
### Variável especial

$_ - Último argumento do ultimo comando executado
<!--more-->

Exemplo:

[![asciicast](https://asciinema.org/a/457158.svg)](https://asciinema.org/a/457158)
