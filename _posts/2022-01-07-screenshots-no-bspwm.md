---
title: "Screenshots no BSPWM"
date: 2022-01-07 13:36:29
image: '/assets/img/'
description:
key: screenshot
tags: bspwm sxhkdrc screenshot
---

Um jeito fácil para realizar screenshots no BSPWM:

<!--more-->

Pacotes necessários:

* scrot
* xclip


### Editando o arquivo sxhkdrc


Adicione ao arquivo:


```
# Screenshot
shift + @Print
        scrot -s '%d-%m-%Y-%T.png' -e 'xclip -select on clipboard -t "image/png" < $f && mv $f ~/Imagens/' && notify-send 'Scrot' 'Captura de tela salva em \n~/Imagens/'
```


Pressionando o shift e a tecla Print Screen, o cursor irá mudar, para selecionar  o que se deseja "Printar". ;)


See you...
