---
title: "Como resetar o GNOME"
date: 2020-07-16 20:47:00
comments: true
key: GNOME
image: '/assets/img/'
description:
tags: gnome
---
O processo é super simples!

Abra o terminal e primeiramente faça o backup do estado atual do GNOME executando:
<!--more-->
```
$ dconf dump / > nome_do_backup
```

Em seguinte, execute o comando que irá resetar o GNOME:

```
$ dconf reset -f /
```

GNOME limpo com as suas configurações padrão! Caso queria retornar para o seu estado anterior, basta restaurar o backup executando:

```
$ dconf load / < nome_do_backup
```

GNOME restaurado com sucesso!

Assista este post em ação no nosso canal do Youtube:

<iframe width="560" height="315" src="https://www.youtube.com/embed/QGPO8bVtv30" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
