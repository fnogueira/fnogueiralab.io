---
title: "Embelezando o BSPWM"
date: 2021-12-25 17:21:40
image: '/assets/img/'
key: bspwm-2
description:
tags: bspwm arch polybar
---

Se você realizou a [instalação básica do BSPWM / Polybar](https://fnogueira.gitlab.io/2021/12/23/instalacao-do-bspwmpolybar-no-arch.html){:target="_blank"} através do nosso site, chegou a hora de deixá-lo mais bonito.

<!--more-->

### Substituto do dmenu

```
sudo pacman -S rofi
```
Após sua instalação, continuemos no terminal:

```
rofi -show drun
```
para escolher um tema para o rofi, execute:

```
rofi-theme-selector
```

Escolha o tema preferido e clique Alt + a para salvar

Em seguida, execute:

```
rofi -show drun -show-icons
```
Após os testes, agora é substituir o dmenu no arquivo sxhkdrc, onde ficará desta forma:

```
# program launcher
super + @space
        rofi -show drun -show-icons
```

### Fontes para o sistema

```
mkdir ~/.local/share/fonts/
```

```
git clone https://github.com/terroo/fonts -b fonts --single-branch
cd fonts/fonts && mv *.ttf *.otf $HOME/.local/share/fonts/
fc-cache -fv
```

###  Temas para o Polybar

```
git clone --depth=1 https://github.com/adi1090x/polybar-themes.git
cd polybar-themes
chmod +x setup.sh
./setup.sh
```

Escolha a opção **1 (Simple)**!

Para trocar os temas, teste da seuinte forma:

```
bash ~/.config/polybar/launch.sh
````

Onde irá aparecer o meio correto:

```
Usage : launch.sh --theme

Available Themes :
--blocks    --colorblocks    --cuts      --docky
--forest    --grayblocks     --hack      --material
--panels    --pwidgets       --shades    --shapes
```
Depois de escolhido o tema, substitua a linha no arquivo bspwmrc:

```
$HOME/.config/polybar/launch.sh --forest
```

No exemplo acima, escolhi o forest!

Se faz necessário editar o arquivo ~/.config/polybar/forest/config.ini e desabilitar o npd (caso não utilize), além de configurar o tema para que apareça fique do seu jeito.
