---
title: "Armazenando seus dotfiles com o git bare"
date: 2022-01-02 20:57:44
image: '/assets/img/'
description:
key: git
tags: git bare dotfiles
---

# Como armazenar seus dotfiles com o git bare

Cenário:

Seus arquivos de configurações da pasta .config, da raiz, etc..., não precisaram ser criados SYMLINKS dos mesmos (Como se é utilizado com o comando stow). Ao se modificar esses arquivos, os mesmos estarão aptos para serem enviados para a nuvem através do git.

<!--more-->

## Iniciando 


```
git init --bare $HOME/.dotfiles
```

`Será criada uma pasta .dotfiles, um repositório git bare (vazio) que será usado para acompanhar os arquivos dotfiles)`


```
alias config='/usr/bin/git --git-dir=$HOME/.dotfiles/ --work-tree=$HOME'
```

`cria um alias chamado config que permite enviar comandos git para o repositório .dotfiles de qualquer local, mesmo fora do repositório.
`

```
echo "alias config='/usr/bin/git --git-dir=$HOME/.dotfiles/ --work-tree=$HOME'" >> $HOME/.zsh/aliases
```
ou, para quem usa o .bashrc:

```
echo "alias config='/usr/bin/git --git-dir=$HOME/.dotfiles/ --work-tree=$HOME'" >> $HOME/.bashrc
```

`Adicionando de forma permanente o alias no zsh ou bash.`

```
config config --local status.showUntrackedFiles no
```

Utilizando o alias config, vamos configurar para o git não mostre o status dos arquivos que não estão sendo acompanhados.


## Adicionando os arquivos


```
config add ~/.bashrc
config commit -m "Adicionando o arquivo .bashrc"
```

`Desta forma, será adicionado o arquivo .bashrc ao repositório com o comentário acima citado
`


## Enviando os arquivos


```
config remote add origin <remote-url>
config push -u origin master
```


`Em <remote-url>, adicione o link do gitlab ou github. Por exemplo:
https://gitlab.com/seu-usuario/repositorio
`


Agora, toda vez que você fizer uma alteração no .bashrc, basta rodar um 'config status' que ele irá mostrar que o arquivo está pronto para ser atualizado no repositório remoto.


Vai adicionando outros arquivos, repetindo o processo na etapa [Adicionando arquivos](https://fnogueira.gitlab.io/2022/01/02/armazenando-seus-dotfiles-com-o-git-bare.html#adicionando-os-arquivos)



Até a próxima!



Fonte: https://www.atlassian.com/git/tutorials/dotfiles