---
title: "Paru: Dez a zero no yay"
date: 2021-02-16 21:34:47
comments: true
key: paru
image: '/assets/img/'
description:
tags: aur arch paru
---
Paru é um AUR Helper desenvolvido por [Morganamilo](https://github.com/morganamilo){:target="_blank"}, que também foi um dos principais desenvolvedores do yay, que é provavelmente o AUR helper mais popular. Ao contrário de yay, que é escrito em Go, o Paru é escrito em Rust.
<!--more-->
Morganamilo anunciou que não iria mais contribuir para o yay e sim se concentraria no paru. Até mesmo [Jguer](https://github.com/Jguer){:target="_blank"}, que é o outro desenvolvedor de yay, comentou que paru é um AUR Helper rico em recursos e, embora ele continue a trabalhar com yay, ainda tinha recursos extensos e úteis.

### Instalando o Paru

Para instalar o Paru, antes é necessário instalar suas dependências:

```
$ sudo pacman -S base-devel git --needed
```

Próximo passo, vamos clonar o diretório:

```
$ git clone https://aur.archlinux.org/paru.git
```

Agora, construir o pacote usando o PKGBUILD:

```
$ cd paru
$ makepkg -si
```

Pronto! Paru instalado com sucesso.

### Comandos úteis do Paru

Comando   | Descrição
--------- | ------
paru < alvo > | Pesquisa e instala interativamente o < alvo >
paru | Alias para paru -Syu
paru -S < alvo > | Instale um pacote específico
paru -Sua | Atualizar pacotes AUR
paru -Qua | Imprime as atualizações  disponíveis do AUR
paru -Qc < alvo > | Imprime os comentários AUR de < alvo >
paru -Ui | Constrói e instala um PKGBUILD no diretório atual
