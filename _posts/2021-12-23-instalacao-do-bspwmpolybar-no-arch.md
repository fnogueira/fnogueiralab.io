---
title: "Instalação do BSPWM / Polybar no Arch"
date: 2021-12-23 17:19:19
key: bspwm
image: '/assets/img/'
description:
tags: bspwm arch polybar
---

## Instalação do BSPWM e Polybar no Arch Linux

<!--more-->

#### Configurações iniciais

```
sudo pacman -S lightdm lightdm-gtk-greeter xorg --needed
```
Depois de instalado, editar o arquivo /etc/lightdm/lightdm.conf

Descomentar a linha #greeter-seesion e deixar a mesma desta forma:

```
greeter-session=lightdm-gtk-greeter
```

#### Habilitando o Lightdm

```
sudo systemctl enable lightdm.service
```

#### Instalando o básico do BSPWM

```
sudo pacman -S bspwm sxhkd picom nitrogen dmenu alacritty arandr git vim archlinux-wallpaper --needed
```

##### Configurando o X Profile

Crie na raiz o arquivo .xprofile e adicione as seguintes linhas:

```
sxhkd &
exec bspwm
```
#### Criando diretórios e configurando os arquivos

```
mkdir -p ~/.config/{bspwm,sxhkd}
```
Em seguida, copiar os arquivos de example:

```
cp /usr/share/doc/bspwm/examples/bnspwmrc ~/.config/bspwm/
cp /usr/share/doc/bspwm/examples/sxhkrc ~/.config/sxhkd/
```
#### Editando os arquivo de configuração

No arquivo sxhkdrc:

Em 'terminal emulator', trocar urxvt por alacritty

No arquivo bspwmrc, adicione a linha de configuração do teclado pt_BR:

```
echo 'setxkbmap -model abnt2 -layout br -variant abnt2' >> ~/.config/bspwm/bspwmrc
```

#### Reinicie a máquina

`reboot
`

#### Configuração da resolução da tela

Executar no terminal o comando 'arandr', clicar em Saidas > Virtual-1 > Resolução e escolher a resolução da sua tela. Salva o arquivo em .screenlayout como monitor.sh

Adicionar o path do caminho do arquivo monitor.sh no arquivo bspwmrc:

```
echo '$HOME/.screenlayout/monitor.sh' >> ~/.config/bspwm/bspwmrc
```

### Configurando o Papel de parede

Executar no terminal:

```
nitrogen /usr/share/backgrounds/archlinux
```
Escolha o papel de parede e clique em Apply!

Em seguida, adicione a seguinte linha no arquivo ~/.config/bspwm/bspwmrc:

```
nitrogen /usr/share/backgrounds/archlinux --random --set-zoom-fill &
picom -f &
```

### Instalação do Paru

```
git clone https://aur.archlinux.org/paru.git
cd paru
makepkg -si
```

### Instalação do Polybar

```
paru -Syy
paru -S polybar --noconfirm --needed
```

### Configuração básica do Polybar

```
mkdir ~/.config/polybar
cp /usr/share/doc/polybar/config ~/.config/polybar
```
Crie um arquivo launch.sh:

```
vim ~/.config/polybar/launch.sh
```
Copie e cole o conteúdo abaixo no arquivo:

```
#!/bin/bash

# Terminate already running bar instances
killall -q polybar
# If all your bars have ipc enabled, you can also use
# polybar-msg cmd quit

# Launch Polybar, using default config location ~/.config/polybar/config
polybar example 2>&1 | tee -a /tmp/polybar.log & disown

echo "Polybar launched..."

```

```
chmod +x ~/.config/polybar/launch.sh
```

Salve o arquivo e adicione a seguinte linha ao arquivo bspwmrc:

```
echo '$HOME/.config/polybar/launch.sh' >> ~/.config/bspwm/bspwmrc
```
### Finalizando...

```
sudo pacman -S neofetch powerline powerline-fonts
```
Adicione as seguintes linhas ao ~/.bashrc:

```
neofetch
powerline-daemon -q
POWERLINE_BASH_CONTINUATION=1
POWERLINE_BASH_SELECT=1
. /usr/share/powerline/bindings/bash/powerline.sh
```

Reinicie e agora é só curtir! ;)
