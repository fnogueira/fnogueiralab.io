---
title: "Picom: Configurando a transparência"
date: 2022-01-02 17:57:38
image: '/assets/img/'
description:
key: picom
tags: bspwm picom
---
## Como configurar a transparência do terminal no Picom

A configuração a seguir, serve para habilitar a transparência em deteminados emuladores de terminais, ou em apenas um, como desejar.

<!--more-->

### Instalação do Picom



```
sudo pacman -S picom
```

### Configuração inicial


```
mkdir ~/.config/picom
cd $_
cp /usr/share/doc/picom/picom.conf.example picom.conf
```

### Configuração final


Editar o arquivo picom.conf recém criado.

Descomentar a seguinte linha:


```
active-opacity = 1.0
```

Em opacity-rule, adicionar as seguintes linhas:


```
opacity-rule = [
    "80:class_g = 'Alacritty' && focused",
    "60:class_g = 'Alacritty' && !focused",
];
```

No exemplo acima, a transparência está habilitada, mas somente a tela do Alacritty ficará com transparência!


### Dicas bônus


Para saber o nome da classe de cada programa, abra uma tela do terminal ao lado da que queira identificar e execute o seguinte comando no terminal:

```
xprop WM_CLASS
```

Ao mudar a seta do cursor, basta clicar na tela do programa, que será exibido a classe do mesmo. Vide o exemplo do Code para o Arch LInux:

```
WM_CLASS(STRING) = "code-oss", "code-oss"
```

Até a próxima!

Ps.: Não esquecer de adicionar a seguinte linha no arquivo bspwrc:


```
picom &
```

Desta forma o picom será carregado na inicialização do seu BSPWM.